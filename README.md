vim-easy-c
==========

A Vim plugin useful for the C language.

Feel free to use it as you want. Read the comments and function names, follow
the yellow brick road.

https://gitlab.gnome.org/swilmet/vim-easy-c

See also
--------

- [vim-swilmet-config](https://gitlab.gnome.org/swilmet/vim-swilmet-config)
