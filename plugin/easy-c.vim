"----------------------------------
" Basic SetupIndentation functions
"----------------------------------

" The length of a real <tab> is unchanged, for this, use tabstop.

" TODO use an argument: SetupIndentationSpaces(n_spaces)
" (I'm far from a Vim expert, as you can see).

function! EasyC_SetupIndentation_2_spaces()
	setlocal expandtab shiftwidth=2 softtabstop=2
endfunction

function! EasyC_SetupIndentation_4_spaces()
	setlocal expandtab shiftwidth=4 softtabstop=4
endfunction

function! EasyC_SetupIndentation_8_spaces()
	setlocal expandtab shiftwidth=8 softtabstop=8
endfunction

"---------------------------
" Highlight Unwanted Spaces
"---------------------------

" Copied from:
" http://vim.wikia.com/wiki/Highlight_unwanted_spaces

" Note that this function is better than using the 'list' and 'listchars'
" options, because when writing the current line, it doesn't get in the way by
" highlighting the trailing space all the time (thanks, probably, to the
" InsertEnter and InsertLeave difference).
" Problem: it looks quite ugly... It's a copy/paste that I don't understand.
function! EasyC_HighlightUnwantedSpaces()
	highlight ExtraWhitespace ctermbg=red guibg=red
	match ExtraWhitespace /\s\+$/
	autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
	autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
	autocmd InsertLeave * match ExtraWhitespace /\s\+$/
	autocmd BufWinLeave * call clearmatches()
endfunction

" To use it, put for example this in your vimrc:
"autocmd Filetype c call EasyC_HighlightUnwantedSpaces()
"autocmd Filetype cpp call EasyC_HighlightUnwantedSpaces()

"--------------------------
" Switch between .c and .h
"--------------------------

" Copied and adapted from somewhere, but I don't remember where.

function! EasyC_SwitchSourceHeader()
	if (expand ("%:t") == expand ("%:t:r") . ".c")
		find %:t:r.h
	else
		find %:t:r.c
	endif
endfunction

" To use it, put for example this in your vimrc to switch with ",s".
"autocmd BufEnter *.c,*.h nmap ,s :call EasyC_SwitchSourceHeader()<CR>

"------------------
" Setup cinoptions
"------------------

" For the indentation and alignment for C code.

" To make it work, the 'cindent' option must be set, which is usually already
" done via the C filetype plugin file.

function! EasyC_Setup_cinoptions_gedit_style()
	setlocal cinoptions=(0,t0
endfunction

" Also for projects like GLib and GTK:
function! EasyC_Setup_cinoptions_GNU_style()
	setlocal cinoptions=>2s,n-s,{s,^-s,:s,(0,t0
endfunction

"----------------------------------------------
" Setup completion, with clang_complete plugin
"----------------------------------------------

" Useful when using the clang_complete plugin:
" https://github.com/xavierd/clang_complete

function! EasyC_SetupCompletion()
	set completeopt=menuone,preview
	set previewheight=2

	" Limit popup menu height
	set pumheight=15

	let g:clang_library_path = "/usr/lib64/"

	let g:clang_complete_macros = 1

	" More efficient, but maybe a bit buggy
	let g:clang_use_library = 1

	let g:clang_complete_auto = 0

	" Easier shortcut (Ctrl+space). 'Down' selects the first item.
	imap <C-@> <C-X><C-U><Down>

	" A hack to make it work with the Meson or CMake build systems.
	if filereadable("./build/compile_commands.json")
		let g:clang_auto_user_options = "compile_commands.json"
		let g:clang_compilation_database = "./build/"
	elseif filereadable("../build/compile_commands.json")
		let g:clang_auto_user_options = "compile_commands.json"
		let g:clang_compilation_database = "../build/"
	elseif filereadable("../../build/compile_commands.json")
		let g:clang_auto_user_options = "compile_commands.json"
		let g:clang_compilation_database = "../../build/"
	endif
endfunction

" To use it, put for example this in your vimrc:
"autocmd Filetype c,cpp call EasyC_SetupCompletion()

"--------------------
" Line up parameters
"--------------------

" This uses the gcu-lineup-parameters program from gdev-c-utils:
" https://gitlab.gnome.org/swilmet/gdev-c-utils/

" gcu-lineup-parameters is independent of Vim and can be wired up in other
" text editors. (Instead of implementing everything with the Vim scripting
" language).

function! EasyC_LineupParametersTabs()
	let l:winview = winsaveview()
	execute "normal {V]]:!gcu-lineup-parameters --tabs\<CR>"
	call winrestview(l:winview)
endfunction

function! EasyC_LineupParametersSpaces()
	let l:winview = winsaveview()
	execute "normal {V]]:!gcu-lineup-parameters\<CR>"
	call winrestview(l:winview)
endfunction

function! EasyC_MapLineupParametersTabs()
	map <F4> :call EasyC_LineupParametersTabs()<CR>
endfunction

function! EasyC_MapLineupParametersSpaces()
	map <F4> :call EasyC_LineupParametersSpaces()<CR>
endfunction

"--------------------
" Setup coding style
"--------------------

function! EasyC_SetupCodingStyle_Common()
	" For comments.
	setlocal textwidth=80 formatoptions=croq
endfunction

function! EasyC_SetupCodingStyle_gedit_style()
	call EasyC_SetupCodingStyle_Common()
	call EasyC_Setup_cinoptions_gedit_style()
	call EasyC_MapLineupParametersTabs()
endfunction

function! EasyC_SetupCodingStyle_GNU_style()
	call EasyC_SetupIndentation_2_spaces()
	call EasyC_SetupCodingStyle_Common()
	call EasyC_Setup_cinoptions_GNU_style()
	call EasyC_MapLineupParametersSpaces()
endfunction

" To use it, put for example this in your vimrc:
"autocmd Filetype c,cpp call EasyC_SetupCodingStyle_gedit_style()
